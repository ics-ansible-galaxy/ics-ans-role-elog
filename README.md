# ics-ans-role-elog

Ansible role to install ELOG.

ELOG is an Electronic Logbook.
See https://midas.psi.ch/elog/index.html for more information.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
elog_rpm: "http://artifactory.esss.lu.se/artifactory/rpm-ics/centos/7/x86_64/elog-3.1.4.2-1.x86_64.rpm"
elog_cert_file: /etc/ssl/certs/esss.lu.se.chained.crt
elog_key_file: /etc/ssl/certs/esss.lu.se.key
elog_url: "https://{{ ansible_fqdn }}"
elog_admin_users:
  - user1
  - user2
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-elog
```

## License

BSD 2-clause
