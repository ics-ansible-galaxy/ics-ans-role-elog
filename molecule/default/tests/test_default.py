import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_elogd_enabled_and_started(host):
    elogd = host.service('elogd')
    assert elogd.is_enabled
    assert elogd.is_running


def test_elog_homepage(host):
    cmd = host.run('curl -Lk https://localhost')
    assert '<title>ELOG - $subject</title>' in cmd.stdout


def test_imagemagick_installed(host):
    cmd = host.run('convert --version')
    assert 'Version: ImageMagick' in cmd.stdout


def test_theme_installed(host):
    assert host.file('/usr/local/elog/themes/Confluence/default.css').exists


def test_elogd_cfg(host):
    assert host.file("/usr/local/elog/elogd.cfg").contains("Admin user = user1, user2")
